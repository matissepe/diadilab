# Diadilab

Diadilab is an application for monitoring and analyzing state transition during an intervention.

## Install prebuild application
Get Windows installer in `dist/` folder

## Install project
### Prerequisite
- ### [Git (Windows only)](https://git-scm.com/)
Install from this [link](https://git-scm.com/download/win)

- ### [Node.js](https://nodejs.org/en/)

For Windows, install from this [link](https://nodejs.org/dist/v14.17.3/node-v14.17.3-x64.msi)

```bash
# Debian
wget -qO- https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt install -y nodejs
```

### Installation

Clone the project
```bash
git clone https://gitlab.com/matissepe/diadilab.git && cd diadilab
```

Install dependences
```bash
npm install
```

### Usage

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

The app will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `dist` folder.\
Binaries for your current OS will be created.