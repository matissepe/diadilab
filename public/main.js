const { ipcMain } = require('electron');
const sqlite3 = require('sqlite3').verbose();
const isDev = require("electron-is-dev");

const dbPath = isDev ? './public/db.sqlite3' : `${__dirname}/db.sqlite3`.replace('app.asar', 'app.asar.unpacked')

const database = new sqlite3.Database(dbPath, (err) => {
    if (err) console.error('Database opening error: ', err);
});

ipcMain.on('state-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('state-reply', (err && err.message) || rows);
    });
});

ipcMain.on('state2-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('state2-reply', (err && err.message) || rows);
    });
});

ipcMain.on('state3-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('state3-reply', (err && err.message) || rows);
    });
});

ipcMain.on('category-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('category-reply', (err && err.message) || rows);
    });
});

ipcMain.on('scenario-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('scenario-reply', (err && err.message) || rows);
    });
});

ipcMain.on('scenario2-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('scenario2-reply', (err && err.message) || rows);
    });
});

ipcMain.on('scenario3-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('scenario3-reply', (err && err.message) || rows);
    });
});

ipcMain.on('assoc-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('assoc-reply', (err && err.message) || rows);
    });
});

ipcMain.on('assoc2-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('assoc2-reply', (err && err.message) || rows);
    });
});

ipcMain.on('sequence-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('sequence-reply', (err && err.message) || rows);
    });
});

ipcMain.on('history-message', (event, sql, params) => {
    database.all(sql, params, (err, rows) => {
        event.reply('history-reply', (err && err.message) || rows);
    });
});