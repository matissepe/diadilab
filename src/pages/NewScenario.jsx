import { Container, TextField, List, ListItem, Divider, ListItemText, ListItemSecondaryAction, IconButton } from '@material-ui/core';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { getStates, getStatesFromScenarioId, endState, startState } from '../controllers/StatesController'
import DeleteIcon from '@material-ui/icons/Delete';
import { getScenarioFromId, insertScenario, updateScenarioFromId } from '../controllers/ScenariosController'
import Matrix from '../components/Matrix'
import '../style.css'

class NewScenario extends Component {
    constructor(props) {
        super(props)
        let scenarioId = parseInt(this.props.match.params.id);

        getStates((res) => this.handleSetAllStates(res))

        if (scenarioId) {
            getScenarioFromId(scenarioId, (res) => this.handleSetScenario(res))
            getStatesFromScenarioId(scenarioId, (res) => this.handleSetStates(res))

            this.state = {
                name: '',
                description: '',
                allStates: [],
                filter: "",
                states: [],
                matrix: []
            }
        }
    }

    state = {
        name: "",
        description: "",
        allStates: [],
        filter: "",
        states: [startState, endState],
        matrix: [[0, 0], [0, 0]]
    }

    handleSetStates = states => {
        states.push(endState)
        states.unshift(startState)
        this.setState({ states })
    }
    handleSetAllStates = allStates => this.setState({ allStates, filter: allStates.length !== 0 ? allStates[0].name : ""})
    handleSetScenario = scenario => this.setState({ name: scenario.name, description: scenario.description, matrix: scenario.matrix })

    handleClearFilter = () => this.setState({ filter: "" })
    handleNameChange = event => this.setState({ name: event.currentTarget.value });
    handleDescriptionChange = event => this.setState({ description: event.currentTarget.value })
    handleSearchStatesList = event => this.setState({ filter: event.currentTarget.value })

    handleSwitchColorMatrix = (x, y) => {
        let colorCode = this.state.matrix[x][y]
        let matrixTemp = this.state.matrix.slice()
        colorCode >= 3 ? matrixTemp[x][y] = 0 : matrixTemp[x][y] = colorCode + 1

        this.setState({ matrix: matrixTemp })
    }

    handleClickSearchStateItem = (id, filteredStates) => {
        if (this.state.states.length - 2 < 12 && id) {

            //Add only if state is not already in the list
            if (this.state.states.findIndex(state => state.id === id) === -1) {

                let statesList = [...this.state.states];
                let matrixTemp = [...this.state.matrix];

                const index = filteredStates.findIndex(state => state.id === id);
                const second2lastIndex = statesList.length - 1
                statesList.splice(second2lastIndex, 0, filteredStates[index])

                let matrixLine = []
                statesList.forEach(() => matrixLine.push(0))
                matrixTemp.splice(second2lastIndex, 0, matrixLine)

                matrixTemp.forEach((line, index) => {
                    if (index !== matrixTemp.length - 2) {
                        line.splice(second2lastIndex, 0, 0)
                    }
                })
                this.setState({ states: statesList, matrix: matrixTemp });
            }
        }
    }

    handleDeleteState = (index) => {
        const statesList = [...this.state.states];
        let matrixTemp = [...this.state.matrix];

        matrixTemp.splice(index, 1)
        for (const m of matrixTemp) m.splice(index, 1)

        statesList.splice(index, 1);
        this.setState({ states: statesList, matrix: matrixTemp });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.states.length !== 0) {

            let states = [...this.state.states]
            states.pop()
            states.shift()

            let scenario = {
                name: this.state.name,
                description: this.state.description,
                matrix: this.state.matrix,
                states
            }
            let scenarioId = parseInt(this.props.match.params.id);
            scenarioId ? updateScenarioFromId(scenarioId, scenario) : insertScenario(scenario)

            let param = this.props.match.params.goBack
            this.props.history.push(`/${param !== undefined ? param : ""}`)
        }
    }

    render() {
        let title = "Ajouter un scénario"
        let subTitle = "Nouveau scénario"

        if (this.props.match.params.id) {
            title = "Modifier un scénario"
            subTitle = "Modifier"
        }

        return (
            <>
                <h2>{title}</h2>
                <form onSubmit={this.handleSubmit} >
                    <Container maxWidth="lg">
                        <Header />
                    </Container>
                    <Container maxWidth="lg" className="newContainer">
                        <p className="newPageTitle" >{subTitle}</p>

                        <TextField required label="Nom" onChange={this.handleNameChange} className="inputForm" value={this.state.name} /><br />
                        <TextField required label="Description" variant="outlined" multiline onChange={this.handleDescriptionChange} className="inputForm" value={this.state.description} /><br />

                        {/**Select states */}

                        <div style={{display: 'flex'}}>
                            <div style={{width: '50%'}} >
                                <TextField type='search' label="Chercher un état..." onChange={this.handleSearchStatesList} value={this.state.filter} style={{ marginBottom: '20px' }} />
                                <SearchStateList filter={this.state.filter} handleClickSearchStateItem={this.handleClickSearchStateItem} states={this.state.states} allStates={this.state.allStates} />
                            </div>

                            <div style={{width: '50%'}} >
                                <p className="statesSelectedTitle" >États sélectionnés ({this.state.states.length - 2}/10)</p>
                                <StateList states={this.state.states} handleDeleteState={this.handleDeleteState} />
                            </div>
                        </div>

                        {/**Matrix */}
                        <div style={{ marginTop: '50px' }}>
                            <p className="matrixTransitionTitle" >Matrice de transition</p>
                            <Matrix states={this.state.states} matrix={this.state.matrix} handleChangeColor={this.handleSwitchColorMatrix} />
                            <Legend />
                        </div>
                    </Container>
                </form>
            </>
        );
    }
}

function Header() {
    return (
        <div style={{display: 'flex'}} >
            <div style={{width: '50%'}}>
                <button className="backButton" onClick={() => window.history.back()}>Retour</button>
            </div>
            
            <div style={{width: '50%'}}>
                <button className="validateButton" type="submit" >Valider</button>
            </div>
        </div>
    )
}

function SearchStateList(props) {
    let filter = props.filter.toUpperCase()
    let clickable = true
    let handleClickSearchStateItem = props.handleClickSearchStateItem
    let states = props.states
    let allStates = props.allStates
    let statesSearch

    if (filter.length === 0) {
        statesSearch = [{ name: "Aucun résultat" }]
        clickable = false
    }
    else {
        statesSearch = allStates.filter((state) => {
            return ((
                state.name.toUpperCase().includes(filter) ||
                state.description.toUpperCase().includes(filter) ||
                state.category.toUpperCase().includes(filter)) &&
                states.findIndex(state2 => state2.id === state.id) === -1
            )
        })
        if (statesSearch.length === 0) {
            statesSearch = [{ name: "Aucun résultat" }]
            clickable = false
        }
    }

    return (
        <List className="searchList">
            {statesSearch.map((state) => {
                return (
                    <>
                        <ListItem button={clickable} onClick={() => handleClickSearchStateItem(state.id, statesSearch)}>
                            <p>{state.name}</p>
                        </ListItem>
                        <Divider />
                    </>
                )
            }).slice(0, 5)}
        </List>
    )
}

function StateList(props) {
    let states = props.states
    let handleDeleteState = props.handleDeleteState

    const ActionButton = (props) => {
        let index = props.index

        return (
            <ListItemSecondaryAction >
                <IconButton edge="end" aria-label="delete" color="inherit" onClick={() => handleDeleteState(index)} >
                    <DeleteIcon style={{color: 'white'}} />
                </IconButton>
            </ListItemSecondaryAction>
        )
    }

    return (
        <List className="statesSelectedList">
            {states.map((state, index) => {
                const actionButton = index !== states.length - 1 && index !== 0 ? <ActionButton index={index} /> : () => <></>
                const divider = index !== states.length - 1 ? <Divider /> : () => <></>

                return (
                    <>
                        <ListItem>
                            <ListItemText>
                                <p>{state.name}</p>
                            </ListItemText>
                            {actionButton}
                        </ListItem>
                        {divider}
                    </>
                )
            })}
        </List>
    )
}

function Legend() {

    return (
        <>
            <p className="matrixLegendTitle">Légende</p>
            <div>
                <div style={{display: 'flex'}}>
                    <div className="matrixLegend matrixBoxGreenEnd" />
                    <p className="matrixLegendRow" >Transition possible</p>
                </div>

                <div style={{display: 'flex'}}>
                    <div className="matrixLegend matrixBoxOrangeEnd" />
                    <p className="matrixLegendRow" >Transition risquée</p>
                </div>

                <div style={{display: 'flex'}}>
                    <div className="matrixLegend matrixBoxRedEnd" />
                    <p className="matrixLegendRow" >Transition très risquée</p>
                </div>

                <div style={{display: 'flex'}}>
                    <div className="matrixLegend matrixBoxGreyEnd" />
                    <p className="matrixLegendRow" >Transition non pertinente</p>
                </div>
            </div>
        </>
    )
}

export default withRouter(NewScenario);