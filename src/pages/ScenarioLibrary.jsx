import React, { useState } from 'react';
import { List, Container, Accordion, AccordionDetails, AccordionSummary, AccordionActions, Divider, Collapse, ListItem } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import { Link } from "react-router-dom";
import { deleteScenarioFromId, getScenariosFromFilter } from '../controllers/ScenariosController'
import { getStatesFromScenarioId } from '../controllers/StatesController'
import { getHistoryFromScenarioId } from '../controllers/HistoryController'
import MatrixDialog from '../components/MatrixDialog'
import '../style.css'

class ScenarioLibrary extends React.Component {

    constructor(props) {
        super(props)
        getScenariosFromFilter("", (scenarios) => this.handleSetScenarios(scenarios))
    }

    state = {
        scenariosList: [],
        expandedPanel: false,
        filter: "",
        openMatrixDialog: false,
        openCommentsDialog: false,
        interventionId: false
    }

    handleSetScenarios = (scenariosList) => this.setState({ scenariosList })
    handleDelete = (id) => {
        const scenariosList = [...this.state.scenariosList];
        const index = scenariosList.findIndex(scenario => scenario.id === id);
        scenariosList.splice(index, 1);
        this.setState({ scenariosList });
        deleteScenarioFromId(id)
    }

    handleAccordionChange = (id) => {
        if (id === this.state.expandedPanel) id = false
        this.setState({ expandedPanel: id });
    };

    handleFilter = event => {
        getScenariosFromFilter(event.currentTarget.value, (scenarios) => this.handleSetScenarios(scenarios))
        this.setState({ filter: event.currentTarget.value })
    }
    handleOpenMatrixDialog = interventionId => this.setState({ openMatrixDialog: true, interventionId })
    handleCloseMatrixDialog = () => this.setState({ openMatrixDialog: false })

    render() {
        let dialog = () => <></>
        if (this.state.openMatrixDialog) dialog = <MatrixDialog handleClose={this.handleCloseMatrixDialog} scenarioId={this.state.expandedPanel} interventionId={this.state.interventionId} />

        return (
            <>
                <h2>Bibliothèque de scénarios</h2>
                <Container maxWidth="lg">
                    <Header handleFilter={this.handleFilter} filter={this.state.filter} />
                    <ScenariosList filter={this.state.filter} scenarios={this.state.scenariosList} handleDelete={this.handleDelete} handleAccordionChange={this.handleAccordionChange} expandedPanel={this.state.expandedPanel} handleOpenMatrixDialog={this.handleOpenMatrixDialog} />
                </Container>
                {dialog}
            </>
        )
    }
}

function Header(props) {
    const handleFilter = props.handleFilter

    return (
        <div style={{display: 'flex'}} >

            <Link to="/" className="link" style={{width: '33%'}}>
                <button>Retour</button>
            </Link>

            <div style={{textAlign: 'center', width: '33%'}}>
                <input type="text" onChange={handleFilter} className="searchBar" placeholder="Rechercher" />
            </div>

                {/**Add state button */}
            <Link to="/newScenario/scenarioLibrary" className="link" style={{width: '33%'}}>
                <button className="fab" ><AddIcon />Créer un scénario</button>
            </Link>

        </div>
    )
}

function ScenariosList(props) {
    let scenarios = props.scenarios
    const handleDelete = props.handleDelete
    const handleAccordionChange = props.handleAccordionChange
    const expandedPanel = props.expandedPanel
    const handleOpenMatrixDialog = props.handleOpenMatrixDialog

    if (scenarios.length === 0) return <p className="emptyListTitle" >Créez votre 1er scénario via le bouton en haut à droite.</p>

    return (
        <List className="list">
            {scenarios.map((scenario) => <ScenarioItem
                scenario={scenario}
                onDelete={handleDelete}
                handleAccordionChange={handleAccordionChange}
                handleOpenMatrixDialog={handleOpenMatrixDialog}
                expandedPanel={expandedPanel} />
            )}
        </List>
    )
}

/**
 * Display an item of the main list
 */
function ScenarioItem(props) {
    const scenario = props.scenario
    const onDelete = props.onDelete
    const expandedPanel = props.expandedPanel
    const handleAccordionChange = props.handleAccordionChange
    const handleOpenMatrixDialog = props.handleOpenMatrixDialog

    let details = () => <></>
    if (expandedPanel === scenario.id) details = <ScenarioItemDetail handleOpenMatrixDialog={handleOpenMatrixDialog} scenario={scenario} />

    return (
        <Accordion button className="accordions" expanded={expandedPanel === scenario.id} onChange={() => handleAccordionChange(scenario.id)}>

            <AccordionSummary
                expandIcon={<ExpandMoreIcon className="expandIcon" />}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <p className="accordionTitle">{scenario.name}</p>
            </AccordionSummary>

            <Divider className="divider"/>
            {details}
            <Divider className="divider"/>

            <AccordionActions>
                <Link to={`/intervention/${scenario.id}`} className="detailsLink" >
                    <button size="medium" className="accordionsButton" >Jouer le scénario</button>
                </Link>
                
                <Link to={`/newScenario/scenarioLibrary/${scenario.id}`} className="detailsLink">
                    <button size="medium" className="accordionsButton" >Modifier</button>
                </Link>

                <button size="medium" className="accordionsButton" onClick={() => onDelete(scenario.id)}>Supprimer</button>
            </AccordionActions>

        </Accordion>
    )
}

function ScenarioItemDetail(props) {
    const handleOpenMatrixDialog = props.handleOpenMatrixDialog
    const scenario = props.scenario

    const [states, setStates] = useState([false])
    const [history, setHistory] = useState([false])

    if (states[0] === false) getStatesFromScenarioId(scenario.id, (res) => setStates(res))
    if (history[0] === false) getHistoryFromScenarioId(scenario.id, (res) => setHistory(res))

    let historyHook = () => <></>
    if (history.length > 0) historyHook = <History history={history} handleOpenMatrixDialog={handleOpenMatrixDialog} />

    return (
        <AccordionDetails>
            <div className="column">
                <p>
                    Description: {scenario.description}<br />
                    Durée théorique: {computeDuration(states)}s
                </p>
            </div>
            <div className="column">
                <StatesList states={states} /> <br />
                {historyHook}
            </div>
        </AccordionDetails>
    )
}

/**
 * Display the sub list of states
 */
function StatesList(props) {
    const states = props.states

    const [open, setOpen] = React.useState(false);
    const handleClick = () => setOpen(!open);

    return (<>
        <button className="detailsButton" onClick={handleClick}>Afficher les états</button>

        <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                <Divider className="divider" style={{marginTop: '10px'}} />
                {states.map((state, index) => {
                    return (
                        <>
                            <ListItem>
                                <p>{`#${index + 1} ${state.name}`}</p>
                            </ListItem>
                            <Divider className="divider"/>
                        </>
                    )
                })}
            </List>
        </Collapse>
    </>
    )
}

/**
 * Display the sub list of the scenario's history
 */
function History(props) {
    const history = props.history

    const [open, setOpen] = useState(false);
    const handleClick = () => setOpen(!open);
    const handleOpenMatrixDialog = props.handleOpenMatrixDialog

    return (
        <>
            <button className="detailsButton" onClick={handleClick}>Afficher l'historique</button>

            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <Divider className="divider" style={{marginTop: '10px'}}/>
                    {history.map((intervention) => {
                        return (
                            <>
                                <ListItem button onClick={() => handleOpenMatrixDialog(intervention.id)} >
                                    <p>{`${intervention.date}: \n${intervention.comment}`}</p>
                                </ListItem>
                                <Divider className="divider"/>
                            </>
                        )
                    })}
                </List>
            </Collapse>
        </>
    )
}

/**
 * Compute the duration of all states in a scenario
 */
function computeDuration(statesList) {
    let duration = 0
    statesList.forEach((state) => duration += state.duration)
    return duration
}

export default ScenarioLibrary;