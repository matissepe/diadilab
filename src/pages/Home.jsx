import React, { useState } from 'react';
import { Link } from "react-router-dom";
import HelpDialog from '../components/HelpDialog'
import HelpIcon from '@material-ui/icons/Help';
import '../style.css'

export default function Home() {
    const [openHelpDialog, setOpenHelpDialog] = useState(false);

    return (
        <>
            <h1 className="homeTitle" >Diadilab</h1>

            <div>
                <Link to="/stateLibrary" className="homeButtonLink" >
                    <button className="homeButton">Créer un état</button><br/>
                </Link>
                
                <Link to="/newScenario" className="homeButtonLink" >
                    <button className="homeButton">Créer un scénario</button><br/>
                </Link>

                <Link to="/scenarioLibrary" className="homeButtonLink" >
                    <button className="homeButton">Bibliothèque de scénarios</button><br/>
                </Link>
            </div>

            <button className="helpButton" onClick={() => setOpenHelpDialog(true)}>
                <HelpIcon className="helpIcon" />
            </button>

            <div className="quitDiv">
                <button className="homeButton quitButton" type="button" onClick={window.close}>Quitter</button>
            </div>

            <OpenDialog open={openHelpDialog} setOpen={setOpenHelpDialog} />
        </>
    );
}

function OpenDialog(props) {
    return props.open ? <HelpDialog setOpen={props.setOpen} /> : <></>
}