import { Container, TextField, MenuItem, Select, InputLabel, ListItemSecondaryAction, IconButton } from '@material-ui/core';
import React, { Component } from 'react';
import { getCategories, insertCategories, safeDeleteCategory } from '../controllers/CategoriesController'
import { withRouter } from 'react-router-dom';
import { insertState, getStateFromId, updateStateFromId } from '../controllers/StatesController'
import CreateCategoryDialog from '../components/CreateCategoryDialog'
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import SafeDeleteDialog from '../components/SafeDeleteDialog';
import '../style.css'

class NewState extends Component {

    constructor(props) {
        super(props)
        let stateId = parseInt(this.props.match.params.id);
        if (stateId) getStateFromId(stateId, (state) => this.handleSetState(state))
        getCategories((res) => this.handleSetCategories(res))
    }

    state = {
        category: "",
        categories: [],
        name: "",
        description: "",
        duration: 1,
        openCreateCategoryDialog: false,
        openSafeDeleteCategoryDialog: false,
        addedCategories: [],
        statesSafeDelete: []
    }

    handleSetCategories = categories => this.setState({ categories: [...categories, {name: "Créer une nouvelle catégorie"}]})
    handleCloseSafeDeleteCategoryDialog = () => this.setState({openSafeDeleteCategoryDialog: false})
    handleCloseCreateCategoryDialog = () => this.setState({openCreateCategoryDialog: false})
    handleSetState = state => this.setState({ name: state.name, description: state.description, category: state.category, duration: state.duration })
    handleNameChange = event => this.setState({ name: event.currentTarget.value });
    handleDescriptionChange = event => this.setState({ description: event.currentTarget.value })

    handleAddCategory = (event, category) => {
        event.preventDefault();
        const categories = [...this.state.categories]
        categories.splice(categories.length - 1, 0, category)

        this.setState({ categories, addedCategories: [...this.state.addedCategories, category], openCreateCategoryDialog: false, category: category.name })
    }

    handleDeleteCategory = (index) => {

        //Safe delete
        //Only delete if the category is not in a state
        safeDeleteCategory(this.state.categories[index].name, (states) => {
            if (states.length > 0) this.setState({openSafeDeleteCategoryDialog: true, statesSafeDelete: states})
            else {
                const categories = [...this.state.categories]
                categories.splice(index, 1)
                this.setState(categories)
            }
        })
    }

    handleCategoryChange = event => {
        const category = event.target.value
        if (category !== this.state.categories[this.state.categories.length - 1].name) this.setState({ category: event.target.value })
        else this.setState({openCreateCategoryDialog: true})
    }

    handleDurationChange = event => {
        const duration = event.currentTarget.value
        if (duration > 0 || !duration ) this.setState({ duration });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        let state = {
            name: this.state.name,
            description: this.state.description,
            category: this.state.category,
            duration: this.state.duration,
        }

        let stateId = parseInt(this.props.match.params.id);
        stateId ? updateStateFromId(stateId, state) : insertState(state)

        if (this.state.addedCategories.length > 0) insertCategories(this.state.addedCategories)

        this.props.history.push(`/${this.props.match.params.goBack}`)
    }

    render() {
        let title = "Ajouter un état"
        let subTitle = "Nouvel état"

        if (this.props.match.params.id) {
            title = "Modifier un état"
            subTitle = "Modifier"
        }

        let dialog = () => <></>
        if (this.state.openCreateCategoryDialog) dialog = <CreateCategoryDialog onClose={this.handleCloseCreateCategoryDialog} onAddCategory={this.handleAddCategory} />
        if (this.state.openSafeDeleteCategoryDialog) dialog = <SafeDeleteDialog onClose={this.handleCloseSafeDeleteCategoryDialog} list={this.state.statesSafeDelete} page='NEW_STATE' />
        
        return (
            <>
                <h2>{title}</h2>
                <form onSubmit={this.handleSubmit} >
                    <Container maxWidth="lg">
                        <Header />
                    </Container>
                    <Container maxWidth="lg" className="newContainer">
                        <Form
                            title={subTitle}
                            name={this.state.name}
                            description={this.state.description}
                            category={this.state.category}
                            duration={this.state.duration}
                            categories={this.state.categories}
                            handleCategoryChange={this.handleCategoryChange}
                            handleDescriptionChange={this.handleDescriptionChange}
                            handleDurationChange={this.handleDurationChange}
                            handleNameChange={this.handleNameChange}
                            handleDeleteCategory={this.handleDeleteCategory} />
                    </Container>
                </form>
                {dialog}
            </>
        );
    }
}

function Header() {
    return (
        <div style={{display: 'flex'}} >
            <div style={{width: '50%'}}>
                <button className="backButton" onClick={() => window.history.back()}>Retour</button>
            </div>
            
            <div style={{width: '50%'}}>
                <button className="validateButton" type="submit" >Valider</button>
            </div>
        </div>
    )
}

function Form(props) {
    let { name, title, description, category, duration, categories, handleCategoryChange, handleDurationChange, handleDescriptionChange, handleNameChange, handleDeleteCategory } = props

    return (
        <>
            <p className="newPageTitle" >{title}</p>

            <TextField required label="Nom" onChange={handleNameChange} className="inputForm" value={name} /><br />
            <TextField required label="Description" variant="outlined" multiline onChange={handleDescriptionChange} className="inputForm"  value={description} /><br />


            <InputLabel>Catégorie</InputLabel>
            <Select
                required
                label="Catégorie"
                select margin="dense"
                value={category}
                onChange={handleCategoryChange}
                className="inputForm"
            >
                {categories.map((categoryParam, index) => {
                    let plusIcon = () => <></>
                    let actionButton = () => <></>
                    if (index === categories.length - 1) plusIcon = <AddIcon />
                    else if (categoryParam.name !== category) actionButton = <ListItemSecondaryAction><IconButton onClick={() => handleDeleteCategory(index) } className="deleteActionButton"> <DeleteIcon/> </IconButton></ListItemSecondaryAction>

                    return (
                        <MenuItem key={categoryParam.name} value={categoryParam.name} >
                            {plusIcon}
                            {categoryParam.name}
                            {actionButton}
                        </MenuItem>
                    )
                })}
            </Select><br />

            <TextField required label="Durée en seconde" onChange={handleDurationChange} type={'number'} value={duration} className="inputForm" /><br />
        </>
    )
}

export default withRouter(NewState);