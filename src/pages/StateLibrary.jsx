import React from 'react';
import { List, Container, Accordion, AccordionDetails, AccordionSummary, AccordionActions, Divider } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import { Link } from "react-router-dom";
import { getScenariosFromIds } from '../controllers/ScenariosController'
import { getStates, safeDeleteStateFromId } from '../controllers/StatesController'
import SafeDeleteDialog from '../components/SafeDeleteDialog'
import '../style.css'

class StateLibrary extends React.Component {

    constructor(props) {
        super(props)
        getStates((states) => this.handleSetStates(states))
    }

    state = {
        statesList: [],
        expandedPanel: false,
        filter: "",
        openSafeDeleteDialog: false,
        scenariosSafeDelete: []
    }

    handleSetStates = (statesList) => this.setState({ statesList })
    handleDelete = (id) => {
        //Safe delete
        //Only delete if the state is not in a scenario
        safeDeleteStateFromId(id, (scenariosIds) => {
            if (scenariosIds.length >= 0) {
                getScenariosFromIds(scenariosIds, (scenarios) => {
                    this.setState({ openSafeDeleteDialog: true, scenariosSafeDelete: scenarios })
                })
            }
        })
    }

    handleCloseSafeDeleteDialog = () => this.setState({ openSafeDeleteDialog: false })

    handleAccordionChange = (id) => {
        if (id === this.state.expandedPanel) id = false
        this.setState({ expandedPanel: id });
    };

    handleFilter = event => this.setState({ filter: event.currentTarget.value })

    render() {
        let dialog = () => <></>
        if (this.state.openSafeDeleteDialog) dialog = <SafeDeleteDialog page='STATE_LIBRARY' list={this.state.scenariosSafeDelete} onClose={this.handleCloseSafeDeleteDialog} />

        return (
            <>
                <h2>Bibliothèque d'états</h2>
                <Container maxWidth="lg">
                    <Header handleFilter={this.handleFilter} filter={this.state.filter} />
                    <StatesList filter={this.state.filter} states={this.state.statesList} handleDelete={this.handleDelete} handleAccordionChange={this.handleAccordionChange} expandedPanel={this.state.expandedPanel} />
                </Container>
                {dialog}
            </>
        )
    }
}

function Header(props) {
    const handleFilter = props.handleFilter

    return (
        <div style={{display: 'flex'}} >

            <Link to="/" className="link" style={{width: '33%'}}>
                <button>Retour</button>
            </Link>

            <div style={{textAlign: 'center', width: '33%'}}>
                <input type="text" onChange={handleFilter} className="searchBar" placeholder="Rechercher" />
            </div>

                {/**Add state button */}
            <Link to="/newState/stateLibrary" className="link" style={{width: '33%'}}>
                <button className="fab" ><AddIcon />Créer un état</button>
            </Link>
      
        </div>
    )
}

function StatesList(props) {
    let states = props.states
    const handleDelete = props.handleDelete
    const handleAccordionChange = props.handleAccordionChange
    const expandedPanel = props.expandedPanel
    const filter = props.filter.toUpperCase()

    if (states.length === 0) return <p className="emptyListTitle" >Créez votre 1er état via le bouton en haut à droite.</p>

    if (filter.length > 0) {
        states = states.filter((state) => {
            return ((
                state.name.toUpperCase().includes(filter) ||
                state.description.toUpperCase().includes(filter) ||
                state.category.toUpperCase().includes(filter))
            )
        })
    }

    return (
        <List className="list">
            {states.map((state) => <StateItem
                key={state.id}
                state={state}
                onDelete={handleDelete}
                handleAccordionChange={handleAccordionChange}
                expandedPanel={expandedPanel}
            />)}
        </List>
    )
}

function StateItem(props) {
    const state = props.state
    const onDelete = props.onDelete
    const expandedPanel = props.expandedPanel
    const handleAccordionChange = props.handleAccordionChange

    return (
        <Accordion button className="accordions" expanded={expandedPanel === state.id} onChange={() => handleAccordionChange(state.id)}>

            <AccordionSummary
                expandIcon={<ExpandMoreIcon className="expandIcon" />}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <p className="accordionTitle">{state.name}</p>
            </AccordionSummary>

            <Divider />

            <AccordionDetails>
                <p>
                    Description: {state.description}<br />
                    Catégorie: #{state.category}<br />
                    Durée: {state.duration}s
                </p>
            </AccordionDetails>

            <Divider />

            <AccordionActions>
                <Link to={`/newState/stateLibrary/${state.id}`} className="detailsLink" >
                    <button className="accordionsButton">Modifier</button>
                </Link>

                <button className="accordionsButton" onClick={() => onDelete(state.id)}>Supprimer</button>
            </AccordionActions>

        </Accordion>
    )
}

export default StateLibrary;