import React, { Component, useState } from 'react';
import { exportComponentAsJPEG } from 'react-component-export-image';
import { withRouter, Link } from 'react-router-dom';
import { Container, List, ListItem, TextField, Dialog, DialogContent, Divider } from '@material-ui/core';
import { getScenarioFromId } from '../controllers/ScenariosController'
import AddIcon from '@material-ui/icons/Add';
import { getStatesFromScenarioId, startState, endState, insertStatesWithHistory, getNextId as getNextStateId } from '../controllers/StatesController'
import { insertHistory, getNextId as getNextHistoryId } from '../controllers/HistoryController'
import '../style.css'
import Matrix from '../components/Matrix'
import StatesTimes from '../components/StatesTimes'
import dateFormat from 'dateformat'
import CommentsDialog from '../components/CommentsDialog';

class Intervention extends Component {

    constructor(props) {
        super(props)

        let scenarioId = parseInt(this.props.match.params.id)
        getScenarioFromId(scenarioId, (res) => this.handleSetScenario(res))
        getStatesFromScenarioId(scenarioId, (res) => this.handleSetStates(res))

        this.state = {
            scenario: { name: '' },
            states: [],
            newStates: [],
            currentState: null,
            currentStateTime: 0,
            tempTime: null,
            transitions: [],
            stateChronometer: null,
            finish: false,
            begin: true,
            openAddStateDialog: false,
            openAddCommentDialog: false,
            openCommentDialog: false,
            comment: ""
        }
    }

    refTimes = React.createRef()
    refMatrix = React.createRef()

    handleSetScenario = (scenario) => this.setState({ scenario })
    handleOpenAddStateDialog = () => {
        clearInterval(this.state.stateChronometer)
        let stateChronometer = setInterval(() => this.increaseTime(), 1000)
        this.setState({ openAddStateDialog: true, stateChronometer, tempTime: this.state.currentStateTime, currentStateTime: 0 })
    }

    handleDisplayComments = (comment) => this.setState({comment: comment, openCommentDialog: true})

    handleInsertNewState = (state) => {
        const states = [...this.state.states]
        const matrix = this.state.scenario.matrix;
        const newStates = [...this.state.newStates]
        const second2lastIndex = states.length - 1
        const transitions = [...this.state.transitions]

        newStates.push(state)
        states.splice(second2lastIndex, 0, state)

        let matrixLine = []
        states.forEach(() => matrixLine.push(0))
        matrix.splice(second2lastIndex, 0, matrixLine)

        matrix.forEach((line, index) => {
            if (index !== matrix.length - 2) {
                line.splice(second2lastIndex, 0, 0)
            }
        })
        if (transitions.length !== 0) transitions[transitions.length - 1].time = this.state.tempTime
        transitions.push({ id: state.id, time: null, comment: null })

        this.setState({ states, newStates, currentState: state, transitions, openAddStateDialog: false })
    }

    handleOpenAddCommentDialog = () => this.setState({openAddCommentDialog: true})
    handleAddComment = (comment) => {
        const transitions = [...this.state.transitions]
        transitions[transitions.length - 1].comment = comment
        this.setState({openAddCommentDialog: false, transitions})
    }

    handleSetStates = (states) => {
        states.push(endState)
        states.unshift(startState)
        this.setState({ states })
    }

    handleChangeCurrentState = (id) => {
        const index = this.state.states.findIndex((state) => state.id === id)
        const currentState = this.state.states[index]
        const transitions = [...this.state.transitions]

        clearInterval(this.state.stateChronometer)
        let stateChronometer = setInterval(() => this.increaseTime(), 1000)

        if (transitions.length !== 0) transitions[transitions.length - 1].time = this.state.currentStateTime
        transitions.push({ id: currentState.id, time: null, comment: null })

        this.setState({ currentState, stateChronometer, currentStateTime: 0, transitions, begin: false, stateComment: null })
    }

    handleFinish = () => {
        clearInterval(this.state.stateChronometer)
        const transitions = [...this.state.transitions]
        
        if (transitions.length !== 0) transitions[transitions.length - 1].time = this.state.currentStateTime
        transitions.push({ id: endState.id, time: null, comment: null })

        this.setState({ transitions, finish: true })
    }

    handleSubmit = (comment) => {
        let date = dateFormat(new Date(), "dd/mm/yyyy")

        insertStatesWithHistory(this.state.newStates)
        insertHistory({
            comment,
            scenario: this.state.scenario.id,
            transitions: this.state.transitions,
            date
        })
        this.props.history.push('/')
    }

    increaseTime = () => {
        let newTime = this.state.currentStateTime + 1
        this.setState({ currentStateTime: newTime })
    }

    render() {
        // Comment Field
        let TopScreen = () => FinishScreen(this.handleSubmit, this.refMatrix, this.refTimes, this.state.scenario.name)

        if (!this.state.finish) {
            TopScreen = () => {
                return (
                    <div style={{display: 'flex', marginBottom: '80px'}}>
                        <CurrentStateScreen state={this.state.currentState} time={this.state.currentStateTime} addComment={this.handleOpenAddCommentDialog} />
                        <StatesList states={this.state.states} changeCurrentState={this.handleChangeCurrentState} handleFinish={this.handleFinish} handleAddState={this.handleOpenAddStateDialog} begin={this.state.begin} />
                    </div >
                )
            }
        }

        let dialog = () => <></>
        if (this.state.openAddStateDialog) dialog = <AddStateDialog insertState={this.handleInsertNewState} newStates={this.state.newStates} />
        else if (this.state.openAddCommentDialog) dialog = <AddCommentDialog addComment={this.handleAddComment} />
        else if (this.state.openCommentDialog) dialog = <CommentsDialog comments={this.state.comment} onClose={() => this.setState({openCommentDialog: false}) } />

        return (
            <div className="interventionContainer" >
                <h2>{this.state.scenario.name}</h2>
                <TopScreen />
                <Matrix states={this.state.states} matrix={this.state.scenario.matrix} transitions={this.state.transitions} handleDisplayComments={this.handleDisplayComments} ref={this.refMatrix} />
                <Container maxWidth="md" style={{ marginTop: '100px' }} >
                    <StatesTimes transitions={this.state.transitions} states={this.state.states} ref={this.refTimes} />
                </Container>
                {dialog}
            </div>
        );
    }
}

function FinishScreen(handleSubmit, refMatrix, refTimes, scenarioName) {

    let comment = ""
    const onChange = event => comment = event.currentTarget.value

    return (
        <Container style={{ backgroundColor: 'white', borderRadius: '10px', padding: '30px', width: '80%', marginBottom: '70px' }} maxWidth="md" align="center">

            <p className="saveInterventionTitle" >Enregister l'intervention avec un commentaire</p>

            <TextField
                label="Commentaire"
                variant="outlined"
                style={{ width: '100%' }}
                rows={4}
                onChange={onChange}
                multiline>
            </TextField><br />

            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}} >
                <button onClick={() => handleSubmit(comment)} className="saveInterventionButton">Enregistrer l'intervention</button>

                <button 
                    onClick={() => exportComponentAsJPEG(refMatrix, { fileName: `${scenarioName}_matrice_${dateFormat(new Date(), "dd/mm/yyyy")}` })}
                    className="saveInterventionButton">
                    Exporter la matrice</button>

                <button 
                    onClick={() => exportComponentAsJPEG(refTimes, { fileName: `${scenarioName}_temps_${dateFormat(new Date(), "dd/mm/yyyy")}` })}
                    className="saveInterventionButton">
                    Exporter les temps</button>

                <Link to="/" className="link" >
                    <button className="saveInterventionButton">Quitter</button>
                </Link>
            </div>

            

        </Container>
    )
}

function CurrentStateScreen(props) {
    const state = props.state
    const time = props.time
    const addComment = props.addComment

    if (!state) return (
        <div style={{width: '50%'}}>
            <p className="clickStateToStart">Cliquez sur début pour commencer</p>
        </div>
    )

    const addCommentButton = state.id !== startState.id ? <button className="addCommentButton" onClick={addComment} >Ajouter un commentaire</button> : () => <></>

    return (
        <div style={{width: '50%'}}>
            <p className="stateInProgress">État en cours</p>
            <p className="currentState" >{state.name}</p>
            <p className="currentStateDescription" >{state.description}</p>
            <p className="chronometer" >{time}s</p>
            {addCommentButton}
        </div>
    )
}

function StatesList(props) {
    const states = props.states
    const changeCurrentState = props.changeCurrentState
    const handleFinish = props.handleFinish
    const handleAddState = props.handleAddState
    const begin = props.begin

    if (begin) {
        return (
            <div style={{width: '50%'}}>
                <button onClick={() => changeCurrentState(startState.id)} className="beginButton" ><p className="beginButtonText" >Début</p></button>
                <button onClick={() => window.history.back()} className="backButtonIntervention" ><p className="backButtonTextIntervention" >Retour</p></button>
            </div>
        )
    }

    let addStateButton = states.length > 1 ? <button className="quickaddState fab" aria-label="add" onClick={handleAddState} variant="extended" ><AddIcon style={{ color: 'white' }} />Ajouter un état</button> : () => <></>

    return (
        <div style={{width: '50%'}}>

            <div style={{display: 'flex', marginBottom: '20px', justifyContent: 'center', alignItems: 'center'}} >
                <p className="interventionStatesListTitle">Liste des états</p>
                {addStateButton}
            </div>
            
            <List className="interventionStatesList" >
                {states.map((state, index) => {
                    let onClick = () => changeCurrentState(state.id)
                    let divider = () => <></>

                    if (state.id === startState.id) return <></>
                    else if (state.id === endState.id) onClick = handleFinish

                    if (index < states.length - 1) divider = <Divider  className="divider2" />

                    return (
                        <>
                        <ListItem button onClick={onClick} >
                            <div className="interventionStatesListItemsDiv" >
                                <p className="interventionStatesListItems">{state.name}:&nbsp;</p>
                                <p className="interventionStatesListItemsDescription">{state.description}</p> 
                            </div>
                        </ListItem>
                        {divider}
                        </>
                    )
                })}
            </List>
        </div>

    );
}

function AddStateDialog(props) {
    const insertState = props.insertState
    const newStates = props.newStates

    const [name, setName] = useState("")
    const [description, setDecription] = useState("")
    const [duration, setDuration] = useState(1)

    const onSubmit = (event) => {
        event.preventDefault();

        getNextHistoryId((historyId) => {
            getNextStateId((stateId) => {
                stateId += newStates.length

                const state = {
                    id: stateId,
                    name,
                    description,
                    duration,
                    category: "",
                    history: historyId
                }
                insertState(state)
            })
        })
    }

    return (
        <Dialog open maxWidth='md' fullWidth>
            <p className="safeDeleteTitle" >Ajout rapide d'un état</p>

            <DialogContent style={{ marginBottom: '20px' }} >
                <form onSubmit={(event) => onSubmit(event)} >
                    <TextField required label="Nom" className="inputForm" style={{ marginBottom: '40px' }} value={name} onChange={(event) => setName(event.currentTarget.value)} /><br />
                    <TextField label="Description" variant="outlined" multiline maxRows='20' className="inputForm" style={{ marginBottom: '20px' }} value={description} onChange={(event) => setDecription(event.currentTarget.value)} /><br />
                    <TextField required label="Durée en seconde" type={'number'} className="dialogInputForm" style={{ marginBottom: '20px' }} value={duration} onChange={(event) => (event.currentTarget.value > 0 || !event.currentTarget.value ) ? setDuration(event.currentTarget.value) : null} /><br />
                    <button type='submit' className="exportButton" variant='contained' >Ajouter</button>
                </form>
            </DialogContent>
        </Dialog>
    )
}


function AddCommentDialog(props) {
    const addComment = props.addComment

    const [comment, setComment] = useState("")

    const onSubmit = (event) => {
        event.preventDefault();
        addComment(comment)
    }

    return (
        <Dialog open maxWidth='md' fullWidth>
            <p className="safeDeleteTitle" >Ajouter un commentaire à cet état</p>

            <DialogContent style={{ marginBottom: '20px' }} >
                <form onSubmit={(event) => onSubmit(event)} >
                    <TextField required multiline maxRows="3" label="Commentaire" className="dialogInputForm" style={{ marginBottom: '40px' }} value={comment} onChange={(event) => setComment(event.currentTarget.value)} /><br />
                    <button type='submit' className="exportButton" variant='contained' >Ajouter</button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default withRouter(Intervention);