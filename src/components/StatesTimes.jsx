import React from 'react';
import { TableContainer, Table, TableRow, TableBody, TableCell, TableHead } from '@material-ui/core';
import { endState } from '../controllers/StatesController'
import '../style.css'

const StatesTimes = React.forwardRef((props, ref) => {
    const transitions = props.transitions
    const states = props.states

    if (!transitions || transitions.length === 0 || !states || states.length === 0) return <></>

    return (
        <TableContainer className="passedStatesTable" ref={ref}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell><p className="timesTableText" >Transition</p></TableCell>
                        <TableCell><p className="timesTableText" >Temps théorique</p></TableCell>
                        <TableCell><p className="timesTableText" >Temps réel</p></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {transitions.map((transition, index) => {
                        if (transition.id === endState.id) return <></>

                        let state = states[states.findIndex((s) => s.id === transition.id)]

                        let duration = ""
                        if (transition.time !== null) duration = transition.time + "s"

                        let nextState = { name: ''}

                        if (index + 1 < transitions.length) nextState = states[states.findIndex((s) => s.id === transitions[index + 1].id)]

                        return (
                            <TableRow>
                                <TableCell>
                                    <p className="timesTableText" >
                                        {state.name} <img src="./arrow_right.png" className="timesTableArrow" alt="" ></img> {nextState.name}
                                    </p>
                                </TableCell>
                                <TableCell><p className="timesTableNumber" >{state.duration}s</p></TableCell>
                                <TableCell><p className="timesTableNumber" >{duration}</p></TableCell>
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    )
})

export default StatesTimes;