import React from 'react';
import { Dialog, DialogContent } from '@material-ui/core';

function CommentsDialog(props) {
    const comments = props.comments
    const onClose = props.onClose

    console.log(comments);

    return (
        <>
            <Dialog open onClose={onClose} maxWidth='md' fullWidth>
                <p className="safeDeleteTitle" >Commentaires</p>

                <DialogContent style={{ marginBottom: '20px' }} >
                    {comments.map((comment) => <p key={comment.transitionId} className="safeDeleteSubTitle">- {comment.transitionId} : {comment.comment}</p>)}
                </DialogContent>
            </Dialog>
        </>
    )
}

export default CommentsDialog;