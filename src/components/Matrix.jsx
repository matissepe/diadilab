import React from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';

const Matrix = React.forwardRef((props, ref) => {
    const states = props.states
    const matrix = props.matrix
    const handleChangeColor = props.handleChangeColor
    const handleDisplayComments = props.handleDisplayComments
    let transitions = props.transitions

    if (states.length === 0) return <></>

    return (
        <>
            <Table className="matrix" ref={ref} >
                <TableHead>
                    <TableRow>
                        <TableCell className="matrixHeadLine">
                            <img src="./arrow_right.png" className="matrixArrow" alt="" ></img>
                        </TableCell>
                        {states.map((state, index) => {
                            let css = "matrixHeadLine"
                            if (index === states.length - 1 ) css = "matrixHeadLineEnd"
                            return <TableCell className={css}><p>{state.name}</p></TableCell>
                        })}
                    </TableRow>
                </TableHead>

                <TableBody>
                    {states.map((stateStart, x) => {
                        let CSS = "matrixHeadLine"
                        if (x === states.length - 1) CSS = "matrixHeadLineStart"

                        return (
                            <TableRow>
                                <TableCell className={CSS}><p>{stateStart.name}</p></TableCell>
                                {states.map((stateEnd, y) => <MatrixCell matrix={matrix} x={x} y={y} handleChangeColor={handleChangeColor} handleDisplayComments={handleDisplayComments} transitions={transitions} stateStart={stateStart} stateEnd={stateEnd} length={states.length} />)}
                            </TableRow>
                        )
                    })}
                </TableBody>
            </Table>
        </>
    )
})

function MatrixCell(props) {
    const matrix = props.matrix
    const length = props.length
    const transitions = props.transitions
    const stateStart = props.stateStart
    const stateEnd = props.stateEnd
    const x = props.x
    const y = props.y
    let letter = ""
    let handleClick
    let comments = []
    let CSSClasse

    if (transitions && transitions.length > 1) {
        transitions.forEach((transition, index) => {
            let id = transition.id
            
            if (id === stateStart.id && transitions.length > (index + 1) && transitions[index + 1].id === stateEnd.id) {
                if (letter.length > 0) letter += '-'
                letter += index + 1
                if (transitions[index + 1].comment) {
                    letter += '*'
                    comments.push({transitionId: index + 1, comment: transitions[index + 1].comment})
                }
            }
        });
    }

    if (props.handleChangeColor) handleClick = () => props.handleChangeColor(x, y)
    else if (comments.length !== 0 && transitions) handleClick = () => props.handleDisplayComments(comments)

    if (matrix && matrix.length > 0) {
        let colorCode = matrix[x][y]

        switch (colorCode) {
            case 1:
                if (length - 1 === y) CSSClasse = "matrixBoxGreenEnd"
                else CSSClasse = "matrixBoxGreen"
                break;
            case 2:
                if (length - 1 === y) CSSClasse = "matrixBoxOrangeEnd"
                else CSSClasse = "matrixBoxOrange"
                break;
            case 3:
                if (length - 1 === y) CSSClasse = "matrixBoxRedEnd"
                else CSSClasse = "matrixBoxRed"
                break;
            default:
                if (length - 1 === y) CSSClasse = "matrixBoxGreyEnd"
                else CSSClasse = "matrixBoxGrey"
                break;
        }
    }
    else CSSClasse = "matrixBoxGrey"

    CSSClasse = `matrixBox ${CSSClasse}`

    return (
        <TableCell className={CSSClasse} onClick={handleClick} >
            <p className="matrixLetter" >{letter}</p>
        </TableCell>
    )
}


export default Matrix;