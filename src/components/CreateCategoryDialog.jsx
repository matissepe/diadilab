import { Dialog, DialogContent, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import '../style.css'

function CreateCategoryDialog(props) {
    const onClose = props.onClose
    const [name, setName] = useState("")

    return (
    <Dialog open onClose={onClose} maxWidth='lg' fullWidth>
        <p className="safeDeleteTitle" >Nouvelle catégorie</p>

        <DialogContent style={{ marginBottom: '20px'}} >
            <form onSubmit={(event) => props.onAddCategory(event, {name})} >
                <TextField required label="Nom" value={name} onChange={(event) => { setName(event.currentTarget.value) }} style={{ width: '80%' }} ></TextField><br />

                <div style={{display: 'flex', marginTop: '10px' }}>
                    <button type="submit" className="exportButton" >Confirmer</button>
                    <button onClick={onClose} className="cancelInput" >Annuler</button>
                </div>
            </form>
        </DialogContent>
    </Dialog>
    )
}

export default CreateCategoryDialog;