import React from 'react';
import { Dialog, DialogContent, Accordion, AccordionDetails, AccordionSummary, AccordionActions, Divider, List, ListItem } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Matrix from './Matrix'
import { endState, startState } from '../controllers/StatesController'

function HelpDialog(props) {
    const setOpen = props.setOpen
    const states = [startState, {name: 'Mon état 1'}, {name: "Mon état 2"}, endState]
    const matrix = [[0, 1, 0, 0], [0, 0, 0, 0], [3, 0, 0, 0], [0, 2, 0, 0]]

    return (
        <>
            <Dialog open onClose={() => setOpen(false)} maxWidth='md' fullWidth>
                <p className="helpDialogTitle" >Comment fonctionne Diadilab</p>

                <DialogContent style={{ marginBottom: '20px' }} >
                <p className="helpDialogSubTitle" >Diadilab permet de créer des procédures composées d'un ensemble d'états et d'analyser la criticité du passage d'un état à l'autre.</p>

                    <p className="helpDialogText" style={{ marginTop: '0' }} >1. Créez-vous une liste d'état.</p>
                    <div style={{ position: 'relative', top: '0', left: '0' }} >
                        <button className="homeButton helpDialogButton">Créer un état</button>
                        <img src="./cursor_left.png" className="cursorBase cursor1" alt="" ></img>
                    </div>

                    <p className="helpDialogText">2. Créez un scénario en y ajoutant l'ensemble des états souhaités.</p>
                    <div style={{ position: 'relative', top: '0', left: '0', marginBottom: '30px' }} >
                        <button className="homeButton helpDialogButton">Créer un scénario</button>
                        <img src="./cursor_right.png" className="cursorBase cursor2" alt="" ></img>
                    </div>
                    <div style={{ position: 'relative', top: '0', left: '0' }}>
                        <p className="helpDialogText">Puis cliquez sur les cases de la matrice pour indiquer ma criticité de chaque transition entre états.</p>
                        <Matrix states={states} matrix={matrix} />
                        <img src="./cursor_right.png" className="cursorBase cursor7" alt="" ></img>
                    </div>

                    <p className="helpDialogText">3. Sélectionnez votre scénario parmi la liste et jouez le scénario.</p>
                    <div style={{ position: 'relative', top: '0', left: '0', marginBottom: '40px' }} >
                        <button className="homeButton helpDialogButton">Bibliothèque de scénarios</button>
                        <img src="./cursor_left.png" className="cursorBase cursor1" alt="" ></img>
                    </div>
                    <div style={{ position: 'relative', top: '0', left: '0' }} >
                        <Scenario withHistory={false} />
                        <img src="./cursor_left.png" className="cursorBase cursor3" alt="" ></img>
                        <img src="./cursor_right.png" className="cursorBase cursor4" alt="" ></img>
                    </div>

                    <p className="helpDialogText">4. Consultez l'historique de vos scénarios et analysez chaque résultats.</p>
                    <div style={{ position: 'relative', top: '0', left: '0' }} >
                        <Scenario withHistory={true} />
                        <img src="./cursor_left.png" className="cursorBase cursor5" alt="" ></img>
                        <img src="./cursor_right.png" className="cursorBase cursor6" alt="" ></img>
                    </div>


                </DialogContent>
            </Dialog>
        </>
    )
}

function Scenario(props) {
    const withHistory = props.withHistory

    let history = <></>
    if (withHistory) {
        history = (
            <List component="div" disablePadding>
                <Divider className="divider" style={{ marginTop: '10px' }} />
                <ListItem>
                    <p>18/05/2021: Mon commentaire</p>
                </ListItem>
                <Divider className="divider" />
                <ListItem>
                    <p>22/05/2021: Super commentaire</p>
                </ListItem>
                <Divider className="divider" />
            </List>
        )
    }

    return (
        <Accordion button className="accordions" expanded>

            <AccordionSummary expandIcon={<ExpandMoreIcon className="expandIcon" />}>
                <p className="accordionTitle">Mon scénario</p>
            </AccordionSummary>

            <Divider className="divider" />
            <AccordionDetails>
                <div className="column">
                    <p>Description: Ma description<br />Durée théorique: 450s</p>
                </div>
                <div className="column">
                    <button className="detailsButton" style={{ marginBottom: '10px' }} >Afficher les états</button>
                    <button className="detailsButton" >Afficher l'historique</button>
                    {history}
                </div>
            </AccordionDetails>
            <Divider className="divider" />

            <AccordionActions>
                <button size="medium" className="accordionsButton" >Jouer le scénario</button>
                <button size="medium" className="accordionsButton" >Modifier</button>
                <button size="medium" className="accordionsButton" >Supprimer</button>
            </AccordionActions>

        </Accordion>
    )
}

export default HelpDialog;