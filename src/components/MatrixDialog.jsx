import { exportComponentAsJPEG } from 'react-component-export-image';
import { Dialog, DialogContent } from '@material-ui/core';
import React, { useState } from 'react';
import { getScenarioFromId } from '../controllers/ScenariosController'
import { getHistoryFromId } from '../controllers/HistoryController'
import { getStatesFromScenarioIdWithHistory, endState, startState } from '../controllers/StatesController'
import Matrix from './Matrix'
import '../style.css'
import StatesTimes from './StatesTimes';
import CommentsDialog from './CommentsDialog'
import dateFormat from 'dateformat'

export default function MatrixDialog(props) {

    const handleClose = props.handleClose
    const scenarioId = props.scenarioId
    const interventionId = props.interventionId

    const [states, setStates] = useState([])
    const [scenario, setScenario] = useState({ name: '' })
    const [intervention, setIntervention] = useState({ date: '' })
    const [matrix, setMatrix] = useState([])
    const [transitions, setTransitions] = useState()
    const [duration, setDuration] = useState(0)

    const [openCommentsDialog, setOpenCommentsDialog] = useState(false)
    const [comments, setComments] = useState()

    const handleDisplayComments = (comments) => {
        setComments(comments)
        setOpenCommentsDialog(true)
    }

    const refMatrix = React.createRef()
    const refTimes = React.createRef()

    if (scenario.name === '') {
        getScenarioFromId(scenarioId, (scenario) => {
            setScenario(scenario)

            getStatesFromScenarioIdWithHistory(scenarioId, interventionId, (res) => {
                const statesList = [...res]
                statesList.push(endState)
                statesList.unshift(startState)

                addMatrixRows(scenario.matrix, statesList, (res) => setMatrix(res))
                setStates(statesList)
            })
        })

        getHistoryFromId(interventionId, (res) => {
            setIntervention(res)
            setTransitions(res.transitions)
            
            let duration = 0
            res.transitions.forEach((t) => duration += t.time)
            setDuration(duration)
        })
    }

    let dialog = () => <></>
    if (openCommentsDialog) dialog = <CommentsDialog comments={comments} onClose={() => setOpenCommentsDialog(false) } />

    return (
        <div>
            <Dialog open={true} onClose={handleClose} maxWidth='lg' fullWidth>
                <p className="safeDeleteTitle" >{scenario.name} - {intervention.date}</p>
                <DialogContent style={{ marginBottom: '20px' }} >
                    <p className="detailsMatrixDialog" >Commentaire: {intervention.comment}</p>
                    <p className="detailsMatrixDialog" style={{ marginBottom: '10px' }} >Durée de l'intervention: {duration}s</p>

                    <div style={{marginBottom: '20px', display: 'flex'}} >
                        <button className="exportButton" onClick={() => exportComponentAsJPEG(refTimes, { fileName: `${scenario.name}_temps_${dateFormat(new Date(), "dd/mm/yyyy")}` })} style={{marginRight: '20px'}} >Exporter les temps</button>
                        <button className="exportButton" onClick={() => exportComponentAsJPEG(refMatrix, { fileName: `${scenario.name}_matrice_${dateFormat(new Date(), "dd/mm/yyyy")}` })} >Exporter la matrice</button>
                    </div>

                    <div style={{ marginBottom: '50px' }} >
                        <Matrix states={states} matrix={matrix} transitions={transitions} handleDisplayComments={handleDisplayComments} ref={refMatrix} />
                        <p className="detailsMatrixDialog">*Transition avec un commentaire. Cliquer pour le voir</p>
                    </div>
                    <StatesTimes transitions={transitions} states={states} ref={refTimes} />
                </DialogContent>
            </Dialog>
            {dialog}
        </div>
    )
}

function addMatrixRows(matrixTemp, statesList, callback) {
    let matrix = [...matrixTemp]

    statesList.forEach((state) => {
        if (state.history != null) {

            let matrixLine = []
            statesList.forEach(() => matrixLine.push(0))
            matrix.splice(matrix.length - 1, 0, matrixLine)

            matrix.forEach((line, index) => {
                if (index !== matrix.length - 2) {
                    line.splice(matrix.length - 2, 0, 0)
                }
            })
        }
    })

    callback(matrix)
}