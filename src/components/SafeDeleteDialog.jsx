import React from 'react';
import { Dialog, DialogContent } from '@material-ui/core';

function SafeDeleteDialog(props) {
    const list = props.list
    const onClose = props.onClose
    const page = props.page

    let text
    if (page === 'STATE_LIBRARY') text = "La suppression de cet état est impossible car les scénarios suivant l'utilisent :"
    else text = "La suppression de cette catégorie est impossible car les états suivant l'utilisent :"

    return (
        <Dialog open onClose={onClose} maxWidth='md' fullWidth>
            <p className="safeDeleteTitle" >Suppression impossible</p>

            <DialogContent style={{ marginBottom: '20px' }} >
                <p className="safeDeleteSubTitle">{text}</p>

                {list.map((item) => <p key={item.name} className="detailsMatrixDialog" style={{ marginLeft: '10px' }} >- {item.name}</p>)}
            </DialogContent>
        </Dialog>
    )
}

export default SafeDeleteDialog;