import Home from './pages/Home'
import ScenarioLibrary from './pages/ScenarioLibrary'
import StateLibrary from './pages/StateLibrary';
import NewScenario from './pages/NewScenario';
import NewState from './pages/NewState'
import Intervention from './pages/Intervention'
import {
  HashRouter as Router,
  Switch,
  Route
} from "react-router-dom";

export default function App() {

  return (
    <>
      <Router>
        <div className="app">
          <Switch>

            <Route path="/" exact component={Home} />
            <Route path="/scenarioLibrary" exact component={ScenarioLibrary} />
            <Route path="/stateLibrary" exact component={StateLibrary} />
            <Route path="/newScenario/:goBack/:id" exact component={NewScenario} />
            <Route path="/newScenario/:goBack" exact component={NewScenario} />
            <Route path="/newScenario" exact component={NewScenario} />
            <Route path="/newState/:goBack/:id" exact component={NewState} />
            <Route path="/newState/:goBack" exact component={NewState} />
            <Route path="/newState" exact component={NewState} />
            <Route path="/intervention/:id" exact component={Intervention} />

          </Switch>
        </div>
      </Router >
    </>
  );
}