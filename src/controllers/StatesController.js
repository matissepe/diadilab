const send = require('./renderer').sendState
const { sendAssoc, sendState2, sendState3 } = require('./renderer')

const getStates = (callback) => {
    send('SELECT * FROM state WHERE history IS NULL')
        .then((res) => callback(res));
}

const getStateFromId = (id, callback) => {
    send('SELECT * FROM state WHERE id = ?', id)
        .then((res) => callback(res[0]));
}

const getStatesFromScenarioId = (scenarioId, callback) => {
    sendState2('SELECT * FROM state, assoc_state_scenario assoc WHERE assoc.scenario = ? AND assoc.state = state.id AND state.history IS NULL', scenarioId)
        .then((res) => callback(res));
}

const getStatesFromScenarioIdWithHistory = (scenarioId, historyId, callback) => {
    sendState2('SELECT DISTINCT id, name, duration, history FROM state, assoc_state_scenario assoc WHERE assoc.scenario = ? AND assoc.state = state.id OR state.history = ?', [scenarioId, historyId])
        .then((res) => callback(res));
}

const insertState = (state) => {
    sendState3('INSERT INTO state (name, category, description, duration) VALUES (?, ?, ?, ?)', [state.name, state.category, state.description, state.duration])
}

const insertStatesWithHistory = (states) => {
    let sql = 'INSERT INTO state (name, category, description, duration, history) VALUES '
    states.forEach((state, index) => {
        sql += `(?, ?, ?, ?, ?)`
        if (index < states.length - 1) sql += ', '
    });
    let params = []
    states.forEach((state) => {
        params.push(state.name)
        params.push(state.category)
        params.push(state.description)
        params.push(state.duration)
        params.push(state.history)
    })

    send(sql, params)
}

const safeDeleteStateFromId = (id, callback) => {
    sendAssoc('SELECT scenario FROM assoc_state_scenario assoc WHERE assoc.state = ?', id)
        .then((scenarios) => {
            if (scenarios.length === 0) {
                send('DELETE FROM state WHERE id = ?', id)
                callback([])
            } else {
                let scenariosIds = scenarios.map((scenario) => scenario.scenario)
                callback(scenariosIds)
            }
        })
}

const updateStateFromId = (id, state) => {
    sendState2('UPDATE state SET name = ?, description = ?, duration = ?, category = ? WHERE id = ?', [state.name, state.description, state.duration, state.category, id])
}

const getNextId = (callback) => {
    send('SELECT seq FROM sqlite_sequence WHERE name = \'state\'')
        .then((id) => {
            id = id[0].seq + 1
            callback(id)
        })
}

const startState = {
    id: -1,
    name: 'Début',
    description: 'Début de l\'intervention',
    duration: 5,
    category: ''
}

const endState = {
    id: -2,
    name: 'Fin',
    description: 'Fin de l\'intervention',
    duration: 5,
    category: ''
}

module.exports = {
    getStates,
    insertState,
    insertStatesWithHistory,
    getStateFromId,
    getStatesFromScenarioIdWithHistory,
    safeDeleteStateFromId,
    updateStateFromId,
    getStatesFromScenarioId,
    getNextId,
    startState,
    endState
}