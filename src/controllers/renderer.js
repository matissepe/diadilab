const electron = window.require('electron');
const { ipcRenderer } = electron;

const sendState = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('state-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('state-message', sql, params);
    });
}

const sendState2 = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('state2-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('state2-message', sql, params);
    });
}

const sendState3 = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('state3-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('state3-message', sql, params);
    });
}

const sendCategory = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('category-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('category-message', sql, params);
    });
}

const sendScenario = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('scenario-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('scenario-message', sql, params);
    });
}

const sendScenario2 = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('scenario2-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('scenario2-message', sql, params);
    });
}

const sendScenario3 = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('scenario3-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('scenario3-message', sql, params);
    });
}

const sendAssoc = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('assoc-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('assoc-message', sql, params);
    });
}

const sendAssoc2 = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('assoc2-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('assoc2-message', sql, params);
    });
}

const sendSqliteSequence = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('sequence-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('sequence-message', sql, params);
    });
}

const sendHistory = (sql, params) => {
    return new Promise((resolve) => {
        ipcRenderer.once('history-reply', (_, arg) => resolve(arg));
        ipcRenderer.send('history-message', sql, params);
    });
}

module.exports = {
    sendState,
    sendState2,
    sendState3,
    sendCategory,
    sendScenario,
    sendScenario2,
    sendScenario3,
    sendHistory,
    sendAssoc,
    sendAssoc2,
    sendSqliteSequence
}