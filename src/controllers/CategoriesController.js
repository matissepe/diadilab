const send = require('./renderer').sendCategory
const { sendState2 } = require('./renderer')

const getCategories = (callback) => {
    send('SELECT * FROM category')
        .then((res) => {
            callback(res)
        });
}

const insertCategories = (categories) => {
    let sql = 'INSERT INTO category (name) VALUES '
    categories.forEach((category, index) => {
        sql += `(?)`
        if (index < categories.length - 1) sql += ', '
    });
    send(sql, categories.map((c) => c.name))
}

const safeDeleteCategory = (name, callback) => {
    sendState2('SELECT name FROM state WHERE category = ?', name)
        .then((res) => {
            if (res.length === 0) send('DELETE FROM category WHERE name = ?', name)
            callback(res)
        })
}

module.exports = {
    getCategories,
    insertCategories,
    safeDeleteCategory
}