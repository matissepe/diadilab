const send = require('./renderer').sendScenario
const { sendAssoc, sendSqliteSequence, sendHistory, sendAssoc2, sendScenario2, sendState, sendScenario3 } = require('./renderer')

const getScenarios = (callback) => {
    send('SELECT * FROM scenario')
        .then((res) => {
            res.forEach((scenario) => scenario.matrix = JSON.parse(scenario.matrix))
            callback(res)
        });
}

const getScenarioFromId = (id, callback) => {
    send('SELECT * FROM scenario WHERE id = ?', id)
        .then((res) => {
            res[0].matrix = JSON.parse(res[0].matrix)
            callback(res[0])
        });
}

const getScenariosFromIds = (ids, callback) => {
    let sql = 'SELECT name FROM scenario WHERE '

    ids.forEach((id, index) => {
        sql += `id = ${id}`
        if (index < ids.length - 1) sql += ' OR '
    })
    send(sql)
        .then((res) => callback(res))
}

const getScenariosFromFilter = (filter, callback) => {
    filter = '%' + filter + '%'
    sendScenario3('SELECT distinct sc.* FROM  scenario sc, assoc_state_scenario ass, state st WHERE sc.id = ass.scenario AND st.id = ass.state AND upper(sc.name) like ? OR upper(sc.description) like ? OR upper(st.name) like ? OR upper(st.description) like ? OR upper(st.category) like ? ', [filter, filter, filter, filter, filter])

    .then((res) => {
        res.forEach((scenario) => scenario.matrix = JSON.parse(scenario.matrix))
        callback(res)
    });
}

const insertScenario = (scenario) => {
    sendScenario2('INSERT INTO scenario (name, description, matrix) VALUES (?, ?, ?)', [scenario.name, scenario.description, JSON.stringify(scenario.matrix)])

    sendSqliteSequence('SELECT seq FROM sqlite_sequence WHERE name = \'scenario\'')
        .then((id) => {
            id = id[0].seq

            let sql = 'INSERT INTO assoc_state_scenario (scenario, state) VALUES '
            scenario.states.forEach((state, index) => {
                sql += `(${id}, ${state.id})`
                if (index < scenario.states.length - 1) sql += ', '
            });
            sendAssoc(sql)
        })
}

const deleteScenarioFromId = (id) => {
    send('DELETE FROM scenario WHERE id = ?', id)
    sendAssoc('DELETE FROM assoc_state_scenario WHERE scenario = ?', id)
    sendHistory('SELECT id FROM history WHERE scenario = ?', id)
        .then((ids) => {
            if (ids.length > 0) {
                let sql = 'DELETE FROM state WHERE '

                ids.forEach((id, index) => {
                    sql += `history = ${id.id}`
                    if (index < ids.length - 1) sql += ' OR '
                })
                sendState(sql)
            }
        })
    sendSqliteSequence('DELETE FROM history WHERE scenario = ?', id)
}

const updateScenarioFromId = (id, scenario) => {
    sendScenario2('UPDATE scenario SET name = ?, description = ? , matrix = ? WHERE id = ?', [scenario.name, scenario.description, JSON.stringify(scenario.matrix), id])
    sendAssoc2(`DELETE FROM assoc_state_scenario WHERE scenario = ?`, id)
        .then(() => {

            let sql = 'INSERT INTO assoc_state_scenario (scenario, state) VALUES '
            scenario.states.forEach((state, index) => {
                sql += `(${id}, ${state.id})`
                if (index < scenario.states.length - 1) sql += ', '
            });
            sendAssoc(sql)
        })

    sendHistory('SELECT id FROM history WHERE scenario = ?', id)
        .then((ids) => {
            if (ids.length > 0) {
                let sql = 'DELETE FROM state WHERE '

                ids.forEach((id, index) => {
                    sql += `history = ${id.id}`
                    if (index < ids.length - 1) sql += ' OR '
                })
                sendState(sql)
            }
        })
    sendSqliteSequence('DELETE FROM history WHERE scenario = ?', id)
}

module.exports = {
    getScenarios,
    insertScenario,
    getScenarioFromId,
    getScenariosFromIds,
    getScenariosFromFilter,
    deleteScenarioFromId,
    updateScenarioFromId
}