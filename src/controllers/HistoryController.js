const send = require('./renderer').sendHistory

const getHistoryFromScenarioId = (id, callback) => {
    send('SELECT * FROM history WHERE scenario = ?', id)
        .then((res) => {
            res.forEach(history => history.transitions = JSON.parse(history.transitions));
            callback(res)
        });
}

const getHistoryFromId = (id, callback) => {
    send('SELECT * FROM history WHERE id = ?', id)
        .then((res) => {
            res[0].transitions = JSON.parse(res[0].transitions)
            callback(res[0])
        })
}

const getNextId = (callback) => {
    send('SELECT seq FROM sqlite_sequence WHERE name = \'history\'')
        .then((id) => {
            id = id[0].seq + 1
            callback(id)
        })
}

const insertHistory = (history) => {
    send('INSERT INTO history (date, comment, transitions, scenario) VALUES (?, ?, ?, ?)', [history.date, history.comment, JSON.stringify(history.transitions), history.scenario])
}

module.exports = {
    getHistoryFromScenarioId,
    getHistoryFromId,
    insertHistory,
    getNextId
}